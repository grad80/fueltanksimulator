#include "singleconnection.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

SingleConnection::SingleConnection(QTcpSocket *socket) : QObject(nullptr), socket(socket)
{
    connect(socket, &QTcpSocket::readyRead, this, &SingleConnection::handleData);
    connect(socket, &QTcpSocket::disconnected, this, &SingleConnection::connectionClosed);
    moveToThread(&thread);
    thread.start();
}

SingleConnection::~SingleConnection()
{
    delete socket;
    socket = nullptr;
    thread.terminate();
    thread.wait();
}

void SingleConnection::handleRegisterOrder(const QJsonObject &orderData)
{
    int tankId = orderData["tankId"].toInt();
    if((tankId < 1) || (tankId > 3)){
        QJsonObject answer;
        answer["type"] = "Answer";
        answer["order"] = "Register";
        answer["tankId"] = tankId;
        answer["status"] = "Tank id not exists";
        QByteArray retAnswer = QJsonDocument(answer).toJson(QJsonDocument::Compact);
        socket->write(retAnswer);
        return;
    }

    QJsonArray parametersList = orderData["parametersList"].toArray();

    RegisteredTankData regData;
    regData.tankId = tankId;
    regData.isProductLevel = parametersList.contains("ProductLevel");
    regData.isTemperature = parametersList.contains("Temperature");
    regData.isDensity = parametersList.contains("Density");
    std::list<RegisteredTankData>::iterator it = find(registeredTankData.begin(), registeredTankData.end(), regData);
    if(it != registeredTankData.end()){
        QJsonObject answer;
        answer["type"] = "Answer";
        answer["order"] = "Register";
        answer["tankId"] = tankId;
        answer["status"] = "Already registered";
        QByteArray retAnswer = QJsonDocument(answer).toJson(QJsonDocument::Compact);
        socket->write(retAnswer);
        return;
    }
    registeredTankData.push_back(regData);
    QJsonObject answer;
    answer["type"] = "Answer";
    answer["order"] = "Register";
    answer["tankId"] = tankId;
    answer["status"] = "OK";
    QByteArray retAnswer = QJsonDocument(answer).toJson(QJsonDocument::Compact);
    socket->write(retAnswer);
}

void SingleConnection::handleUnregisterOrder(const QJsonObject &orderData)
{
    int tankId = orderData["tankId"].toInt();
    auto it = find(registeredTankData.begin(), registeredTankData.end(), tankId);
    if(it == registeredTankData.end()){
        QJsonObject answer;
        answer["type"] = "Answer";
        answer["order"] = "Unregister";
        answer["tankId"] = tankId;
        answer["status"] = "Not registered";
        QByteArray retAnswer = QJsonDocument(answer).toJson(QJsonDocument::Compact);
        socket->write(retAnswer);
        return;
    }
    registeredTankData.remove(tankId);
    QJsonObject answer;
    answer["type"] = "Answer";
    answer["order"] = "Unregister";
    answer["tankId"] = tankId;
    answer["status"] = "OK";
    QByteArray retAnswer = QJsonDocument(answer).toJson(QJsonDocument::Compact);
    socket->write(retAnswer);
}

void SingleConnection::handleData()
{
    QByteArray data = socket->readAll();

    if(data.isEmpty())
        return;

    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(data, &err);
    if(!doc.isNull() && doc.isObject()){
        QJsonObject orderData = doc.object();

        // Register
        const bool isRegisterOrder = orderData.contains("type") && orderData.contains("order") && orderData.contains("tankId") && orderData.contains("parametersList")
                && orderData.value("type").toString() == "Request" && orderData.value("order").toString() == "Register";
        if(isRegisterOrder){
            handleRegisterOrder(orderData);
            return;
        }

        // Unregister
        const bool isUnregisterOrder = orderData.contains("type") && orderData.contains("order") && orderData.contains("tankId") &&
                orderData["type"].toString() == "Request" && orderData["order"].toString() == "Unregister";
        if(isUnregisterOrder){
            handleUnregisterOrder(orderData);
            return;
        }
    }
}

void SingleConnection::currentFuelTankDetails(int fuelTankId, const QDateTime time, const double productLevel, const double temperature, const double density)
{
    Q_UNUSED(time)

    for(auto it = registeredTankData.begin(); it != registeredTankData.end(); ++it){
        if(it->tankId == fuelTankId){
            QJsonObject answer;
            answer["type"] = "Server";
            answer["order"] = "SimulationData";
            answer["tankId"] = fuelTankId;
            QJsonObject data;
            if(it->isProductLevel)
                data["productLevel"] = productLevel;
            if(it->isTemperature)
                data["temperature"] = temperature;
            if(it->isDensity)
                data["density"] = density;
            answer["data"] = data;

            QByteArray retAnswer = QJsonDocument(answer).toJson(QJsonDocument::Compact) + "\r\n";
            socket->write(retAnswer);
        }
    }
}
