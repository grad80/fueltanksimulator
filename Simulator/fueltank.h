#ifndef FUELTANK_H
#define FUELTANK_H

#include <QObject>
#include <atomic>

class FuelTank : public QObject
{
    Q_OBJECT

public:
    static constexpr double PRODUCT_LEVEL_MIN = 0;
    static constexpr double PRODUCT_LEVEL_MAX = 30000;
    static constexpr double TEMPERATURE_MIN = 5;
    static constexpr double TEMPERATURE_MAX = 20;
    static constexpr double DENSITY_MIN = 878;
    static constexpr double DENSITY_MAX = 884;

    FuelTank(int id);
    FuelTank(int id, double productLevel, double temperature, double density);
    FuelTank(const FuelTank &other);

    bool operator ==(const FuelTank &other);
    FuelTank &operator =(const FuelTank &other);

    void setProductLevel(const double value);
    double getProductLevel() const;

    void setTemperature(const double value);
    double getTemperature() const;

    void setDensity(const double value);
    double getDensity() const;

    bool isProductLevelMax() const;
    bool isProductLevelMin() const;
    bool increaseProductLevel(const double step);
    bool decreaseProductLevel(const double step);

    bool isTemperatureMax() const;
    bool isTemperatureMin() const;
    bool increaseTemperature(const double step);
    bool decreaseTemperature(const double step);

    bool isDensityMax() const;
    bool isDensityMin() const;
    bool increaseDensity(const double step);
    bool decreaseDensity(const double step);

    void setEmpty();
    void setAllowedValues(double productLevelMin = PRODUCT_LEVEL_MIN,
            double productLevelMax = PRODUCT_LEVEL_MAX,
            double temperatureMin = TEMPERATURE_MIN,
            double temperatureMax = TEMPERATURE_MAX,
            double densityMin = DENSITY_MIN,
            double densityMax = DENSITY_MAX);
    int getFuelTankId() const;

private:
    std::atomic_int fuelTankId{0};
    double productLevel{PRODUCT_LEVEL_MIN};// mm
    double temperature{TEMPERATURE_MIN};// degrees Celsius
    double density{DENSITY_MIN};// kg/m3

    struct{
        double productLevelMin{PRODUCT_LEVEL_MIN};
        double productLevelMax{PRODUCT_LEVEL_MAX};
        double temperatureMin{TEMPERATURE_MIN};
        double temperatureMax{TEMPERATURE_MAX};
        double densityMin{DENSITY_MIN};
        double densityMax{DENSITY_MAX};
    }allowedValues;
};

#endif // FUELTANK_H
