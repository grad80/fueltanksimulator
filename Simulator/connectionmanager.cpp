#include "connectionmanager.h"
#include <QDebug>

ConnectionManager::ConnectionManager() : QObject(nullptr), tcpServer(this)
{
    connect(&thread, &QThread::started, this, &ConnectionManager::initServer);
    moveToThread(&thread);
    thread.start();
}

ConnectionManager::~ConnectionManager()
{
    thread.terminate();
    thread.wait();
    tcpServer.close();
}

void ConnectionManager::setSimulators(const std::list<std::shared_ptr<FuelTankSimulator>> sim)
{
    listOfSimulators = sim;
}

void ConnectionManager::initServer()
{
    tcpServer.listen(QHostAddress::Any, 10000);
    connect(&tcpServer, &QTcpServer::newConnection, this, [this](){
        QTcpSocket *socket = tcpServer.nextPendingConnection();
        SingleConnection *conn = new SingleConnection(socket);
        qDebug("New connection from: %s:%d", socket->peerAddress().toString().toUtf8().data(), socket->peerPort());
        connect(conn, &SingleConnection::connectionClosed, this, [this](){
            SingleConnection *client = dynamic_cast<SingleConnection *>(sender());
            if(client != nullptr){
                connList.remove(client);
                delete client;
                qDebug("Connection closed");
            }
        });
        for(auto it = listOfSimulators.begin(); it != listOfSimulators.end(); ++it)
            connect(it->get(), &FuelTankSimulator::currentFuelTankDetails, conn, &SingleConnection::currentFuelTankDetails);
        connList.push_back(conn);
    });
}
