#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include <QObject>
#include <QTcpServer>
#include <QThread>
#include "singleconnection.h"
#include <list>
#include "fueltanksimulator.h"

class ConnectionManager : public QObject
{
    Q_OBJECT
public:
    ConnectionManager();
    virtual ~ConnectionManager() override;
    void setSimulators(const std::list<std::shared_ptr<FuelTankSimulator>> sim);

signals:

public slots:
    void initServer();

private:
    QTcpServer tcpServer;
    QThread thread;
    std::list<SingleConnection *> connList;
    std::list<std::shared_ptr<FuelTankSimulator>> listOfSimulators;
};

#endif // CONNECTIONMANAGER_H
