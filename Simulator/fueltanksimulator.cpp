#include "fueltanksimulator.h"

FuelTankSimulator::FuelTankSimulator(int tankId) : QObject(nullptr), simulationTimer(this), fuelTank(tankId)
{
    connect(&simulationTimer, &QTimer::timeout, this, [this](){
        processSimulation();
    });

    connect(this, &FuelTankSimulator::startStopSimulation, this, [this](bool isStarted){
        if(isStarted && !simulationTimer.isActive()){
            fuelTank.setEmpty();
            fuelTank.setAllowedValues(simulationParams.productLevelMin, simulationParams.productLevelMax,
                                      simulationParams.temperatureMin, simulationParams.temperatureMax,
                                      simulationParams.densityMin, simulationParams.densityMax);
            simulationTimer.start(1e3);// 1-second interval
            emit currentFuelTankDetails(this->getTankId(), QDateTime::currentDateTime(), fuelTank.getProductLevel(), fuelTank.getTemperature(), fuelTank.getDensity());
        }else if(!isStarted && simulationTimer.isActive()){
            simulationTimer.stop();
        }
    });

    moveToThread(&thread);
    thread.start();
}

FuelTankSimulator::~FuelTankSimulator()
{
    thread.terminate();
    thread.wait();
    if(simulationTimer.isActive())
        simulationTimer.stop();
}

void FuelTankSimulator::startSimulation()
{
    emit startStopSimulation(true, QPrivateSignal());
}

void FuelTankSimulator::stopSimulation()
{
    emit startStopSimulation(false, QPrivateSignal());
}

int FuelTankSimulator::getTankId() const
{
    return fuelTank.getFuelTankId();
}

void FuelTankSimulator::processSimulation()
{
    simulateProductLevel();
    simulateTemperature();
    simulateDensity();

    emit currentFuelTankDetails(fuelTank.getFuelTankId(), QDateTime::currentDateTime(), fuelTank.getProductLevel(), fuelTank.getTemperature(), fuelTank.getDensity());
}

void FuelTankSimulator::simulateProductLevel()
{
    if(!productLevelTimer.isValid())
        productLevelTimer.start();
    if(productLevelTimer.hasExpired(5e3)){// five seconds interval
        productLevelTimer.start();

        switch(productLevelDirection)
        {
        case SimulationDirection::UPWARDS:
            fuelTank.increaseProductLevel(PRODUCT_LEVEL_STEP);
            if(fuelTank.getProductLevel() >= simulationParams.productLevelMax)
                productLevelDirection = SimulationDirection::DOWNWARDS;
            break;

        case SimulationDirection::DOWNWARDS:
            fuelTank.decreaseProductLevel(PRODUCT_LEVEL_STEP);
            if(fuelTank.getProductLevel() <= simulationParams.productLevelMin)
                productLevelDirection = SimulationDirection::UPWARDS;
            break;
        }
    }
}

void FuelTankSimulator::simulateTemperature()
{
    if(!temperatureTimer.isValid())
        temperatureTimer.start();

    if(temperatureTimer.hasExpired(60e3)){// one minute interval
        temperatureTimer.start();

        switch(temperatureDirection)
        {
        case SimulationDirection::UPWARDS:
            fuelTank.increaseTemperature(TEMPERATURE_STEP);
            if(fuelTank.getTemperature() >= simulationParams.temperatureMax)
                temperatureDirection = SimulationDirection::DOWNWARDS;
            break;

        case SimulationDirection::DOWNWARDS:
            fuelTank.decreaseTemperature(TEMPERATURE_STEP);
            if(fuelTank.getTemperature() <= simulationParams.temperatureMin)
                temperatureDirection = SimulationDirection::UPWARDS;
            break;

        default:
            break;
        }
    }
}

void FuelTankSimulator::simulateDensity()
{
    if(!densityTimer.isValid())
        densityTimer.start();

    if(densityTimer.hasExpired(60e3)){// one minute interval
        densityTimer.start();

        switch(densityDirection) {
        case SimulationDirection::UPWARDS:
            fuelTank.increaseDensity(DENSITY_STEP);
            if(fuelTank.getDensity() >= simulationParams.densityMax)
                densityDirection = SimulationDirection::DOWNWARDS;// change direction to oposite
            break;

        case SimulationDirection::DOWNWARDS:
            fuelTank.decreaseDensity(DENSITY_STEP);
            if(fuelTank.getDensity() <= simulationParams.densityMin)
                densityDirection = SimulationDirection::UPWARDS;// change direction to oposite
            break;

        default:
            break;
        }
    }
}
