#include "consolehelper.h"
#include <QDateTime>

ConsoleHelper::ConsoleHelper() : QObject(nullptr)
{

}

void ConsoleHelper::fuelTankDetailsHandler(int fuelTankId, const QDateTime time, const double productLevel, const double temperature, const double density)
{
    printf("tankId=%d: %s: productLevel=%6.1lf temperature=%3.1lf density=%3.1lf\n",
           fuelTankId, time.toString("hh:mm:ss").toUtf8().data(), productLevel, temperature, density);
}
