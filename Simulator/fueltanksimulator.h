#ifndef FUELTANKSIMULATOR_H
#define FUELTANKSIMULATOR_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QDateTime>
#include <list>
#include "fueltank.h"

class FuelTankSimulator : public QObject
{
    Q_OBJECT

public:
    enum class SimulationDirection
    {
        UPWARDS,
        DOWNWARDS
    };

    const double PRODUCT_LEVEL_STEP = 1000 / 12;// every 5 seconds; 1000 for one minute
    const double TEMPERATURE_STEP = 1;
    const double DENSITY_STEP = 1;

    FuelTankSimulator(int tankId);
    virtual ~FuelTankSimulator();

    void startSimulation();
    void stopSimulation();
    int getTankId() const;

private:
    QThread thread;
    QTimer simulationTimer;
    FuelTank fuelTank;
    SimulationDirection productLevelDirection = SimulationDirection::UPWARDS;
    QElapsedTimer productLevelTimer;
    SimulationDirection temperatureDirection = SimulationDirection::UPWARDS;
    QElapsedTimer temperatureTimer;
    SimulationDirection densityDirection = SimulationDirection::UPWARDS;
    QElapsedTimer densityTimer;
    struct{
        double productLevelMin{15000};
        double productLevelMax{29000};
        double temperatureMin{13};
        double temperatureMax{23};
        double densityMin{FuelTank::DENSITY_MIN};
        double densityMax{FuelTank::DENSITY_MAX};
    }simulationParams;

    void processSimulation();
    void simulateProductLevel();
    void simulateTemperature();
    void simulateDensity();

signals:
    void startStopSimulation(bool isStarted, QPrivateSignal);
    void currentFuelTankDetails(int fuelTankId, const QDateTime time, const double productLevel, const double temperature, const double density);

private slots:
};

#endif // FUELTANKSIMULATOR_H
