#ifndef SINGLECONNECTION_H
#define SINGLECONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QThread>
#include <QDateTime>

class SingleConnection : public QObject
{
    Q_OBJECT
public:
    SingleConnection(QTcpSocket *socket);
    virtual ~SingleConnection() override;

signals:
    void connectionClosed();

private:
    struct RegisteredTankData{
        int tankId{0};
        bool isProductLevel{false};
        bool isTemperature{false};
        bool isDensity{false};

        RegisteredTankData(){
        }

        RegisteredTankData(int id){
            tankId = id;
        }

        bool operator ==(const RegisteredTankData &other){
            if(&other == this)
                return true;
            return tankId == other.tankId;
        }
    };

    QTcpSocket *socket;
    QThread thread;
    std::list<RegisteredTankData> registeredTankData;

    void handleRegisterOrder(const QJsonObject &orderData);
    void handleUnregisterOrder(const QJsonObject &orderData);

private slots:
    void handleData();

public slots:
    void currentFuelTankDetails(int fuelTankId, const QDateTime time, const double productLevel, const double temperature, const double density);
};

#endif // SINGLECONNECTION_H
