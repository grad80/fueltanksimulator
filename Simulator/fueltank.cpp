#include "fueltank.h"

constexpr double FuelTank::PRODUCT_LEVEL_MIN;
constexpr double FuelTank::PRODUCT_LEVEL_MAX;
constexpr double FuelTank::TEMPERATURE_MIN;
constexpr double FuelTank::TEMPERATURE_MAX;
constexpr double FuelTank::DENSITY_MIN;
constexpr double FuelTank::DENSITY_MAX;

FuelTank::FuelTank(int id) : QObject(nullptr), fuelTankId(id)
{

}

FuelTank::FuelTank(int id, double productLevel, double temperature, double density) : QObject(nullptr), fuelTankId(id)
{
    setProductLevel(productLevel);
    setTemperature(temperature);
    setDensity(density);
}

FuelTank::FuelTank(const FuelTank &other) : QObject(nullptr)
{
    if(&other == this)
        return;

    this->productLevel = other.productLevel;
    this->temperature = other.temperature;
    this->density = other.density;
}

bool FuelTank::operator ==(const FuelTank &other)
{
    if(&other == this)
        return true;
    return this->productLevel == other.productLevel
            && this->temperature == other.temperature
            && this->density == other.density;
}

FuelTank &FuelTank::operator =(const FuelTank &other)
{
    if(&other == this)
        return *this;

    this->productLevel = other.productLevel;
    this->temperature = other.temperature;
    this->density = other.density;
    this->allowedValues = other.allowedValues;
    return *this;
}

void FuelTank::setProductLevel(const double value)
{
    productLevel = std::max(std::min(value, allowedValues.productLevelMax), allowedValues.productLevelMin);
}

double FuelTank::getProductLevel() const
{
    return productLevel;
}

void FuelTank::setTemperature(const double value)
{
    temperature = std::max(std::min(value, allowedValues.temperatureMax), allowedValues.temperatureMin);
}

double FuelTank::getTemperature() const
{
    return temperature;
}

void FuelTank::setDensity(const double value)
{
    density = std::max(std::min(value, allowedValues.densityMax), allowedValues.densityMin);
}

double FuelTank::getDensity() const
{
    return density;
}

bool FuelTank::isProductLevelMax() const
{
    return productLevel >= allowedValues.productLevelMax;
}

bool FuelTank::isProductLevelMin() const
{
    return productLevel <= allowedValues.productLevelMin;
}

bool FuelTank::increaseProductLevel(const double step)
{
    if(step < 0)
        return false;

    if(!isProductLevelMax()){
        setProductLevel(productLevel + step);
        return true;
    }
    return false;
}

bool FuelTank::decreaseProductLevel(const double step)
{
    if(step < 0)
        return false;
    if(!isProductLevelMin()){
        setProductLevel(productLevel - step);
        return true;
    }
    return false;
}

bool FuelTank::isTemperatureMax() const
{
    return temperature >= allowedValues.temperatureMax;
}

bool FuelTank::isTemperatureMin() const
{
    return temperature <= allowedValues.temperatureMin;
}

bool FuelTank::increaseTemperature(const double step)
{
    if(!isTemperatureMax()){
        setTemperature(temperature + step);
        return true;
    }
    return false;
}

bool FuelTank::decreaseTemperature(const double step)
{
    if(!isTemperatureMin()){
        setTemperature(temperature - step);
        return true;
    }
    return false;
}

bool FuelTank::isDensityMax() const
{
    return density >= allowedValues.densityMax;
}

bool FuelTank::isDensityMin() const
{
    return density <= allowedValues.densityMin;
}

bool FuelTank::increaseDensity(const double step)
{
    if(!isDensityMax()){
        setDensity(density + step);
        return true;
    }
    return false;
}

bool FuelTank::decreaseDensity(const double step)
{
    if(!isDensityMin()){
        setDensity(density - step);
        return true;
    }
    return false;
}

void FuelTank::setEmpty()
{
    *this = FuelTank(fuelTankId);
}

void FuelTank::setAllowedValues(double productLevelMin, double productLevelMax, double temperatureMin, double temperatureMax, double densityMin, double densityMax)
{
    allowedValues.productLevelMin = std::max(std::min(productLevelMin, PRODUCT_LEVEL_MAX), PRODUCT_LEVEL_MIN);
    allowedValues.productLevelMax = std::max(std::min(productLevelMax, PRODUCT_LEVEL_MAX), PRODUCT_LEVEL_MIN);

    allowedValues.temperatureMin = std::max(std::min(temperatureMin, TEMPERATURE_MAX), TEMPERATURE_MIN);
    allowedValues.temperatureMax = std::max(std::min(temperatureMax, TEMPERATURE_MAX), TEMPERATURE_MIN);

    allowedValues.densityMin = std::max(std::min(densityMin, DENSITY_MAX), DENSITY_MIN);
    allowedValues.densityMax = std::max(std::min(densityMax, DENSITY_MAX), DENSITY_MIN);

    productLevel = allowedValues.productLevelMin;
    temperature = allowedValues.temperatureMin;
    density = allowedValues.densityMin;
}

int FuelTank::getFuelTankId() const
{
    return fuelTankId;
}
