#ifndef CONSOLEHELPER_H
#define CONSOLEHELPER_H

#include <QObject>
#include <QDateTime>

class ConsoleHelper : public QObject
{
    Q_OBJECT
public:
    explicit ConsoleHelper();

public slots:
    void fuelTankDetailsHandler(int fuelTankId, const QDateTime time, const double productLevel, const double temperature, const double density);

};

#endif // CONSOLEHELPER_H
