#include <QCoreApplication>
#include "fueltanksimulator.h"
#include "consolehelper.h"
#include <QDateTime>
#include <list>
#include <memory>
#include "connectionmanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::unique_ptr<ConsoleHelper> consoleHelper(new ConsoleHelper());
    std::list<std::shared_ptr<FuelTankSimulator>> simList;
    for(int i=0; i<3; ++i){
        std::shared_ptr<FuelTankSimulator> simulator(new FuelTankSimulator(i+1));
        QObject::connect(simulator.get(), &FuelTankSimulator::currentFuelTankDetails, consoleHelper.get(), &ConsoleHelper::fuelTankDetailsHandler);
        simulator->startSimulation();
        simList.push_back(simulator);
        simulator.reset();
    }
    std::unique_ptr<ConnectionManager> server(new ConnectionManager());
    server->setSimulators(simList);
    return a.exec();
}
