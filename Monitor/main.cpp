#include <QCoreApplication>
#include "monitorobject.h"
#include "consolehelper.h"
#include <QThread>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    MonitorObject mon;
    ConsoleHelper helper;
    QObject::connect(&mon, &MonitorObject::simDataEvent, &helper, &ConsoleHelper::simDataHandler);
    mon.connectToHost("127.0.0.1", 10000);
    mon.registerTankData(1, {Params::PRODUCT_LEVEL, Params::TEMPERATURE, Params::DENSITY});
    QThread::sleep(1);
    mon.registerTankData(3, {Params::PRODUCT_LEVEL, Params::TEMPERATURE});

    return a.exec();
}
