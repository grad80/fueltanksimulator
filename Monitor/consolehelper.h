#ifndef CONSOLEHELPER_H
#define CONSOLEHELPER_H

#include <QObject>

class ConsoleHelper : public QObject
{
    Q_OBJECT
public:
    explicit ConsoleHelper(QObject *parent = nullptr);

public slots:
    void simDataHandler(int tankId, double productLevel, double temperature, double density);
signals:

};

#endif // CONSOLEHELPER_H
