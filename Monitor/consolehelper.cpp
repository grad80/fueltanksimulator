#include "consolehelper.h"

ConsoleHelper::ConsoleHelper(QObject *parent) : QObject(parent)
{

}

void ConsoleHelper::simDataHandler(int tankId, double productLevel, double temperature, double density)
{
    QString dataToShow = QString("tankId=%1").arg(QString::number(tankId));

    if(productLevel > 0)
        dataToShow += " productLevel=" + QString::number(productLevel, 'f', 1);
    if(temperature > 0)
        dataToShow += " temperature=" + QString::number(temperature, 'f', 1);
    if(density > 0)
        dataToShow += " density=" + QString::number(density, 'f', 1);

    printf("%s\n", dataToShow.toUtf8().data());
}
