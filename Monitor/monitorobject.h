#ifndef MONITOROBJECT_H
#define MONITOROBJECT_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>

enum class Params{
    PRODUCT_LEVEL,
    TEMPERATURE,
    DENSITY
};
Q_DECLARE_METATYPE(Params)

class MonitorObject : public QObject
{
    Q_OBJECT
public:
    MonitorObject();
    void connectToHost(QString ip, quint16 port);
    void registerTankData(int tankId, std::list<Params> listOfParams);

signals:
    void connectToHostEvent(QString ip, quint16 port, QPrivateSignal);
    void simDataEvent(int tankId, double productLevel, double temperature, double density);
    void registerTankDataEvent(int tankId, std::list<Params> listOfParams, QPrivateSignal);

private slots:
    void connectToHostHandler(QString ip, quint16 port);
    void handleData();
    void registerTankDataHandler(int tankId, std::list<Params> listOfParams);

private:
    QThread thread;
    QTcpSocket socket;

    QByteArray prepareRegDataForTank(int tankId, std::list<Params> listOfParamsToRegister);
    void parseFrame(const QByteArray &data);

};


#endif // MONITOROBJECT_H
