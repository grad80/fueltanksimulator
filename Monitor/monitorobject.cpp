#include "monitorobject.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QCoreApplication>

MonitorObject::MonitorObject() : QObject(nullptr), socket(this)
{
    qRegisterMetaType<std::list<Params>>();

    connect(this, &MonitorObject::connectToHostEvent, this, &MonitorObject::connectToHostHandler);
    connect(this, &MonitorObject::registerTankDataEvent, this, &MonitorObject::registerTankDataHandler);
    connect(&socket, &QTcpSocket::readyRead, this, &MonitorObject::handleData);
    moveToThread(&thread);
    thread.start();
}

void MonitorObject::connectToHost(QString ip, quint16 port)
{
    emit connectToHostEvent(ip, port, QPrivateSignal());
}

void MonitorObject::registerTankData(int tankId, std::list<Params> listOfParams)
{
    emit registerTankDataEvent(tankId, listOfParams, QPrivateSignal());
}

void MonitorObject::connectToHostHandler(QString ip, quint16 port)
{
    if(socket.state() == QTcpSocket::ConnectedState)
        return;
    socket.connectToHost(ip, port);
    if(!socket.waitForConnected(5e3))
        return;
}

void MonitorObject::handleData()
{
    static QByteArray data;
    data += socket.readAll();

    int curlyBraceCount{0};
    bool wasFrame;
    do{
        wasFrame = false;
        int i{0};
        while(i < data.size()){
            if(i > 0 && data[i] == '{' && curlyBraceCount == 0){
                data.remove(0, i);
                i = 0;
                continue;
            }
            if(data[i] == '{')
                ++curlyBraceCount;
            else if(data[i] == '}'){
                --curlyBraceCount;
                if(curlyBraceCount == 0){
                    QByteArray buf = data.left(i+1);
                    data.remove(0, i+1);
                    parseFrame(buf);
                    wasFrame = true;
                    break;
                }
            }
            ++i;
        }
    }while(wasFrame);
}

void MonitorObject::registerTankDataHandler(int tankId, std::list<Params> listOfParams)
{
    auto data = prepareRegDataForTank(tankId, listOfParams);
    socket.write(data);
    socket.waitForBytesWritten();
}

QByteArray MonitorObject::prepareRegDataForTank(int tankId, std::list<Params> listOfParamsToRegister)
{
    QJsonObject reg;
    reg["type"] = "Request";
    reg["order"] = "Register";
    reg["tankId"] = tankId;

    QJsonArray params;
    if(std::find(listOfParamsToRegister.begin(), listOfParamsToRegister.end(),Params::PRODUCT_LEVEL) != listOfParamsToRegister.end())
        params.append("ProductLevel");
    if(std::find(listOfParamsToRegister.begin(), listOfParamsToRegister.end(),Params::TEMPERATURE) != listOfParamsToRegister.end())
        params.append("Temperature");
    if(std::find(listOfParamsToRegister.begin(), listOfParamsToRegister.end(),Params::DENSITY) != listOfParamsToRegister.end())
        params.append("Density");
    reg["parametersList"] = params;

    QJsonDocument doc;
    doc.setObject(reg);
    return doc.toJson();
}

void MonitorObject::parseFrame(const QByteArray &data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if(doc.isNull())
        return;

    if(!doc.isObject())
        return;

    QJsonObject obj = doc.object();
    if(!obj.contains("type") || !obj.contains("order") || !obj.contains("tankId"))
        return;

    QString type = obj["type"].toString();
    QString order = obj["order"].toString();
    int tankid = obj["tankId"].toInt();

    if(type == "Answer" && order == "Register"){
        // data regeistered
    }
    else if(type == "Answer" && order == "Unregister"){
        // data unregistered
    }
    else if(type == "Server" && order == "SimulationData"){
        // simulation data
        if(obj.contains("data")){
            QJsonObject simData = obj["data"].toObject();
            QString dataToShow;
            for(auto item : {QStringLiteral("productLevel"), QStringLiteral("temperature"), QStringLiteral("density")}){
                if(simData.contains(item)){
                    if(!dataToShow.isEmpty())
                        dataToShow += ", ";
                    dataToShow += item + ":" + QString::number(simData[item].toDouble(), 'f', 1);
                }
            }
            double productLevel{0};
            double temperature{0};
            double density{0};
            if(simData.contains("productLevel"))
                productLevel = simData["productLevel"].toDouble();
            if(simData.contains("temperature"))
                temperature = simData["temperature"].toDouble();
            if(simData.contains("density"))
                density = simData["density"].toDouble();
            emit simDataEvent(tankid, productLevel, temperature, density);
        }
    }
}
