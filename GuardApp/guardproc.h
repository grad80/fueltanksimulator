#ifndef GUARDPROC_H
#define GUARDPROC_H

#include <QObject>
#include <QProcess>
#include <QThread>

class GuardProc : public QObject
{
    Q_OBJECT
public:
    GuardProc(QString appPath);
    void start();

signals:
    void startEvent();

private slots:
    void startHandler();

private:
    QString appPathToGuard;
    QProcess proc;
    QThread thrd;
};

#endif // GUARDPROC_H
