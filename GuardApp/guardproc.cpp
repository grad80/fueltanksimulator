#include "guardproc.h"
#include <QStringList>

GuardProc::GuardProc(QString appPath) : QObject(nullptr), proc(this)
{
    appPathToGuard = appPath;
    proc.setProgram(appPath);
    proc.setProcessChannelMode(QProcess::ForwardedChannels);
    connect(&proc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, [this](int exitCode, QProcess::ExitStatus exitStatus){
        Q_UNUSED(exitCode)
        if(exitStatus == QProcess::CrashExit)
            proc.start();
    });
    connect(this, &GuardProc::startEvent, this, &GuardProc::startHandler);

    moveToThread(&thrd);
    thrd.start();
}

void GuardProc::start()
{
    emit startEvent();
}

void GuardProc::startHandler()
{
    proc.start();
}
