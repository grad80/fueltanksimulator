#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFile>
#include <QProcess>
#include "guardproc.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Guard other app and restart it when killed or crushed");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(
                {{"p", "program-path"}, "Path to program to guard", "appName"}
                );
    parser.process(a);

    if(!parser.isSet("p") || !QFile::exists(parser.value("program-path")))
        parser.showHelp(1);

    GuardProc gProc(parser.value("program-path"));
    gProc.start();


    return a.exec();
}
